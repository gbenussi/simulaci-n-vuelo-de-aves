# Base laboratorio 3 sedis #

## Manejar el comportamiento de las aves ##
En el archivo *ave.cpp*:

```
#!c++

void main(){
	int avance_x = 1;
	int avance_y = 1;


	while(1){
		//avance_x = rand() % MAXIMO_DESPLAZAMIENTO - MAXIMO_DESPLAZAMIENTO / 2;
		//avance_y = rand() % MAXIMO_DESPLAZAMIENTO - MAXIMO_DESPLAZAMIENTO / 2;
		avance_x = 1;
		avance_y = 1;

		int x_anterior = x;
		int y_anterior = y;

		if(this->x + avance_x >= MAX_X){
			this->x = MIN_X;
		}
		this->x += avance_x;

		if(this->y + avance_y >= MAX_Y){
			this->y = MIN_Y;
		}
		this->y += avance_y;

		direccion = -atan2(destino_x - x, destino_y - y) * 180 / PI;
		
		//printf("(%d, %d) %f\n", x, y, direccion);
		unsigned miliseconds = 5;
		usleep(miliseconds * 1000000);
	}
}
```
En la función main sacada del archivo aves.cpp se puede modificar el comportamiento del ave, es decir, dirigir su vuelo.

## Cambiar representación visual de las aves ##
En el archivo main.cpp, específicamente en la función *draw* se puede modificar lo que dibuja el programa para representar a las aves, aquí está el código de tal función:

```
#!c++

void draw(){
	int x;
	int y;
	int direccion;
  	for (int i = 0; i < Aves.size(); i++)
  	{
  		x 		  = Aves.at(i)->x;
  		y 		  = Aves.at(i)->y;
  		direccion = Aves.at(i)->direccion;
    	glColor3f(1, 1, 1); // blanco
  		
  		// Se supone que en este punto, opengl dibujara cualquier cosa respecto a 0, 0, 0
  		// Por lo tanto, le decimos que rote en "direccion" grados los dibujos y los traslate a la posicion x, y, 0
  		// ** Nota: OpenGl al parecer procesa los eventos alrevez, por lo que se ejecutaria la rotacion
  		// ** y luego la traslacion
  		glTranslatef(x, y, 0.0f);
  		glRotatef(direccion, 0, 0, 1);

  		/*
  			Se mantienen estas proporciones:
	  			Base: 1
  				Altura: 1.9364916731
  				Lado (isosceles): 2
  		*/
  		glBegin(GL_TRIANGLES); // Inicio del dibujo
			glVertex3f(-2.5, -4.841225, 0); // Primer vertice
	      	glVertex3f( 2.5, -4.841225, 0); // Segundo vertice
	      	glVertex3f( 0,  4.841225, 0); // Tercer vertice
	    glEnd(); // Fin del dibujo

	    // Deshago las operaciones de rotacion y translacion
	    glRotatef(-direccion, 0, 0, 1);
	   	glTranslatef(-x, -y, 0.0f);
	 	
		// Le indico una direccion de destino ficticia (en este caso,
		// la esquina superior derecha de la pantalla)
		Aves.at(i)->destino_x = 640;
		Aves.at(i)->destino_y = 640;
  	}
}
```