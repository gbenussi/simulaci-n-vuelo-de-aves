#include <uC++.h>

#include <cmath>
#include <vector>
#include <algorithm>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "punto.cpp"

#define STEP_ROTACION 10
#define MAXIMO_DESPLAZAMIENTO 2
#define PI 3.1416

#define WIN_WIDTH  640
#define WIN_HEIGHT  640
#define PADDING_X  30
#define PADDING_Y  30

#define PERCEPCION_AVE 50 // Rango en el cual el ave puede ver (circulo de radio 50)
#define FUERZA_MAXIMA 0.03
#define DISTANCIA_MINIMA_DE_SEPARACION_DESEADA 25 // Las aves intentarán matener una distancia de 25 pixeles entre ellas
#define VELOCIDAD_DESEADA 2
#define VELOCIDAD_MAXIMA 2

#define MAX_X WIN_WIDTH - PADDING_X // limite derecho hasta donde un ave puede llegar horizontalmente
#define MAX_Y WIN_HEIGHT - PADDING_Y // limite superior hasta donde un ave puede llegar verticalmente
#define MIN_X PADDING_X
#define MIN_Y PADDING_Y
using namespace std;

_Task Ave
{
	protected:
		void main(){
			while(true){

				Punto * Separacion = new Punto(0, 0);
				Punto * Cohesion   = new Punto(0, 0);
				Punto * Alineacion = new Punto(0, 0);

				Separacion = GetSeparacion();
				Cohesion = GetCohesion();
				Alineacion = GetAlineacion();

				// Sumamos las 3 fuerzas obtenidas
				double suma_fuerzas_x = Separacion->x * w_s + Cohesion->x * w_c + Alineacion->x * w_a;
				double suma_fuerzas_y = Separacion->y * w_s + Cohesion->y * w_c + Alineacion->y * w_a;

				// Verificamos que la fuerza resultante no sobrepase el limite permitido
				if(abs(suma_fuerzas_x) > FUERZA_MAXIMA){
					suma_fuerzas_y = FUERZA_MAXIMA * signo(suma_fuerzas_x);
				}

				if(abs(suma_fuerzas_y) > FUERZA_MAXIMA){
					suma_fuerzas_y = FUERZA_MAXIMA * signo(suma_fuerzas_y);
				}

				// Sumamos las fuerzas a la velocidad
				this->velocidad.x += suma_fuerzas_x;
				this->velocidad.y += suma_fuerzas_y;

				// Limitamos la velocidad a la velocidad maxima
				if(abs(this->velocidad.x) > VELOCIDAD_MAXIMA){
					velocidad.x = VELOCIDAD_MAXIMA * signo(velocidad.x);
				}
				if(abs(this->velocidad.y) > VELOCIDAD_MAXIMA){
					velocidad.y = VELOCIDAD_MAXIMA * signo(velocidad.y);
				}

				double posicion_nueva_x = posicion.x + velocidad.x;
				double posicion_nueva_y = posicion.y + velocidad.y;
				
				// Si se sale de los limites, la posicionamos en el lado opuesto
				// (Para dar la sensacion de que sale por un lado y entre por el otro)
				if(posicion_nueva_x > MAX_X)
					posicion_nueva_x = MIN_X;
				if(posicion_nueva_x < MIN_X)
					posicion_nueva_x = MAX_X;

				if(posicion_nueva_y > MAX_Y)
					posicion_nueva_y = MIN_Y;
				if(posicion_nueva_y < MIN_Y)
					posicion_nueva_y = MAX_Y;

				// Calculamos la direccion hacia la cual debe mirar
		        direccion = -atan2(posicion_nueva_x - posicion.x, posicion_nueva_y - posicion.y) * 180 / PI;

		        // Asignamos la posicion nueva
				this->posicion.x = posicion_nueva_x;
				this->posicion.y = posicion_nueva_y;

				// Se duerme el hilo por 15 ms
				unsigned miliseconds = 15;
				usleep(miliseconds * 1000000);
			}
		}

		Punto * GetSeparacion(){
			Punto * Separacion = new Punto(0,0);
			double suma_repulsion_x = 0;
			double suma_repulsion_y = 0;
			double total_repulsion = 0;
			for (int j = 0; j < Aves->size(); ++j)
			{
				double distancia = Punto::Distancia(this->posicion, Aves->at(j)->posicion);
				// Si están demasiado cerca
				if(j != pid && distancia <= DISTANCIA_MINIMA_DE_SEPARACION_DESEADA){
					double distancia_x = -Aves->at(j)->posicion.x + posicion.x;
					double distancia_y = -Aves->at(j)->posicion.y + posicion.y;
					double R_pid_j_x = (distancia_x != 0) ? (distancia_x) / abs(distancia_x) : 0;
					double R_pid_j_y = (distancia_y != 0) ? (distancia_y) / abs(distancia_y) : 0;

					if(distancia == 0){
						R_pid_j_x = FUERZA_MAXIMA * signo(velocidad.x);
						R_pid_j_y = FUERZA_MAXIMA * signo(velocidad.y);
					}
					else{
						R_pid_j_x = distancia_x == 0 ? FUERZA_MAXIMA * signo(velocidad.x) : R_pid_j_x / distancia;
						R_pid_j_y = distancia_y == 0 ? FUERZA_MAXIMA * signo(velocidad.y) : R_pid_j_y / distancia;
					}
					total_repulsion++;

					suma_repulsion_x += R_pid_j_x;
					suma_repulsion_y += R_pid_j_y;
				}
			}

			if(total_repulsion > 0){
				Separacion->x = suma_repulsion_x / total_repulsion;
				Separacion->y = suma_repulsion_y / total_repulsion;
				Separacion = Normalizar(Separacion);
			}
			return Separacion;
		}

		Punto * GetCohesion(){
			Punto * Cohesion = new Punto(0,0);
			double cohesion_suma_x  = 0;
			double cohesion_suma_y  = 0;
			int    cohesion_total   = 0;

			for (int j = 0; j < Aves->size(); ++j)
			{
				if(j != pid && Punto::Distancia(this->posicion, Aves->at(j)->posicion) < PERCEPCION_AVE){
					cohesion_suma_x += Aves->at(j)->posicion.x - posicion.x;
					cohesion_suma_y += Aves->at(j)->posicion.y - posicion.y;
					cohesion_total++;
				}
			}
			if(cohesion_total > 0){
				Cohesion->x = cohesion_suma_x / cohesion_total;
				Cohesion->y = cohesion_suma_y / cohesion_total;
				Cohesion = Normalizar(Cohesion);
			}
			return Cohesion;
		}

		Punto * GetAlineacion(){
			Punto * Alineacion = new Punto(0, 0);
			double suma_velocidad_x = 0;
			double suma_velocidad_y = 0;
			double cantidad_alineacion = 0;

			for (int j = 0; j < Aves->size(); ++j)
			{
				if(j != pid && Punto::Distancia(this->posicion, Aves->at(j)->posicion) < PERCEPCION_AVE){
					suma_velocidad_x += Aves->at(j)->velocidad.x;
					suma_velocidad_y += Aves->at(j)->velocidad.y;
					cantidad_alineacion++;
				}
			}
			if(cantidad_alineacion > 0){
				double promedio_x = suma_velocidad_x / cantidad_alineacion;
				double promedio_y = suma_velocidad_y / cantidad_alineacion;

				Alineacion->x = promedio_x - velocidad.x;
				Alineacion->y = promedio_y - velocidad.y;
				Alineacion = Normalizar(Alineacion);
			}
			return Alineacion;
		}

		int signo(int variable){
			if(variable >= 0)
				return 1;
			return -1;
		}
		Punto * Normalizar(Punto * punto){
			double fuerza_a_sumar_x = punto->x;
			double fuerza_a_sumar_y = punto->y;
			double maximo = abs(fuerza_a_sumar_x) > abs(fuerza_a_sumar_y) ? abs(fuerza_a_sumar_x) : abs(fuerza_a_sumar_y);
			if(maximo == 0){
				return new Punto(0, 0);
			}

			// 1
			if( fuerza_a_sumar_x != 0)
				fuerza_a_sumar_x /= maximo;
			if( fuerza_a_sumar_y != 0)
				fuerza_a_sumar_y /= maximo;

			// 2
			fuerza_a_sumar_x *= VELOCIDAD_DESEADA;
			fuerza_a_sumar_y *= VELOCIDAD_DESEADA;

			// 3
			fuerza_a_sumar_x = fuerza_a_sumar_x - velocidad.x;
			fuerza_a_sumar_y = fuerza_a_sumar_y - velocidad.y;

			// 4
			fuerza_a_sumar_x = min(fuerza_a_sumar_x, (double)FUERZA_MAXIMA);
			fuerza_a_sumar_y = min(fuerza_a_sumar_y, (double)FUERZA_MAXIMA);

			return new Punto(fuerza_a_sumar_x, fuerza_a_sumar_y);
		}
	public:
		double w_s;
		double w_c;
		double w_a;
		vector<Ave*> * Aves;
		Punto posicion;
		Punto velocidad;
		int pid;
		double direccion;

		Ave(int x, int y, int pid, vector<Ave*> * Aves, double ws, double wc, double wa){
			this->w_s = ws;
			this->w_c = wc;
			this->w_a = wa;
			this->posicion.x = x;
			this->posicion.y = y;
			this->Aves = Aves;
			
			double posicion_nueva_x = posicion.x + velocidad.x;
			double posicion_nueva_y = posicion.y + velocidad.y;
			
			if(posicion_nueva_x > MAX_X)
				posicion_nueva_x = MIN_X;
			if(posicion_nueva_x < MIN_X)
				posicion_nueva_x = MAX_X;

			velocidad.x = rand() % (2 * VELOCIDAD_DESEADA) - VELOCIDAD_DESEADA;
			velocidad.y = rand() % (2 * VELOCIDAD_DESEADA) - VELOCIDAD_DESEADA;
			this->pid = pid;
		}
};