#ifndef PUNTO
#define PUNTO
#include <cmath>

class Punto
{
public:
	float x;
	float y;
	Punto(){
		x = 0;
		y = 0;
	}

	Punto(float x, float y){
		this->x = x;
		this->y = y;
	}
	
	static float Distancia(Punto a, Punto b){
		return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
	}

};
#endif