#include <uC++.h>
#include <getopt.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <vector>
#include "ave.cpp"
#include "punto.cpp"

#include <iostream>
using namespace std;

#define RADIO_CREACION 200

void setup();
void display();
void draw();

vector<Ave*> Aves;

// drawing box square
void draw(){
	double x;
	double y;
	int direccion;
	glColor3f(1, 0, 0); // blanco
  	for (int i = 0; i < Aves.size(); i++)
  	{  		
  		x 		  = Aves.at(i)->posicion.x;
  		y 		  = Aves.at(i)->posicion.y;
  		direccion = Aves.at(i)->direccion;

    	glColor3f(1, 1, 1); // blanco

        glTranslatef(x, y, 0.0f);
        glRotatef(direccion, 0, 0, 1);

        /*
            Se mantienen estas proporciones:
                Base: 1
                Altura: 1.9364916731
                Lado (isosceles): 2
        */
        glBegin(GL_TRIANGLES); // Inicio del dibujo
            glVertex3f(-2.5, -4.841225, 0); // Primer vertice
            glVertex3f( 2.5, -4.841225, 0); // Segundo vertice
            glVertex3f( 0,  4.841225, 0); // Tercer vertice
        glEnd(); // Fin del dibujo

        // Deshago las operaciones de rotacion y translacion
        glRotatef(-direccion, 0, 0, 1);
        glTranslatef(-x, -y, 0.0f);
  	}
}

void dibujarLineasRojas(){
	glColor3f(1, 0, 0); // verde
    glBegin(GL_LINES);
        // Lineas Verticales
	    	// Inferior
	        glVertex2f(PADDING_X, PADDING_Y);
	        glVertex2f(WIN_WIDTH - PADDING_X, PADDING_Y);

	        // Superior
	        glVertex2f(PADDING_X, WIN_HEIGHT - PADDING_Y);
	        glVertex2f(WIN_WIDTH - PADDING_X, WIN_HEIGHT - PADDING_Y);

	    // Lineas Horizontales
	        // Izquierda
	        glVertex2f(PADDING_X, WIN_HEIGHT - PADDING_Y);
	        glVertex2f(PADDING_X, PADDING_Y);

	        // Derecha
	        glVertex2f(WIN_WIDTH - PADDING_X, WIN_HEIGHT - PADDING_Y);
	        glVertex2f(WIN_WIDTH - PADDING_X, PADDING_Y);

    glEnd();
}

void setup(){
    glClearColor(0.0, 0.0, 0.0, 1.0); // Color de fondo (negro)
    gluOrtho2D(0, WIN_WIDTH, 0, WIN_HEIGHT);
}

void display(){
    while(1){
	    glClear(GL_COLOR_BUFFER_BIT);
	    glColor3f(1.0, 0.0, 0.0);

	    dibujarLineasRojas();
    	draw();

	    
	    glFlush();
	    //sleepcp(1000);
	    //sleep(1);
    }
}

void handleKeypress(int key, int x, int y) {
	printf("Se presiono la tecla %c (%d)\n", key, key);
    switch (key) {
            case 27: //Escape key                                                                                                                                       
              exit(0); //Exit the program
    }
	glutPostRedisplay();
}

void uMain::main() {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowPosition(300, 0); // Posicion de la ventana en pixeles
    glutInitWindowSize(WIN_WIDTH, WIN_HEIGHT); // Tamano de la ventana en pixeles
    glutCreateWindow("Aves"); // Titulo de la ventana
    glutDisplayFunc(display); // display es la funcion que

    int numero_de_aves = 50;
    double ws = 0.6;
    double wc = 0.2;
    double wa = 0.2;

    int opcion;

   //variables que guardaran los parametros ingresados por pantalla
   const char* cantidadAves = NULL; 
   const char* w_s = NULL;
   const char* w_c = NULL;
   const char* w_a = NULL;

   //Estructura necesaria para el getopt   
   static struct option opcionLarga[] = {
       {"Numeroaves",1,NULL,'N'},
       {"ws",1,NULL,'s'},
       {"wc",1,NULL,'c'},
       {"wa",1,NULL,'a'},
       {NULL,0,NULL,0}
   };

   //Se recorren todas las opciones ingresadas en este ciclo while
   while((opcion = getopt_long(argc, argv, "N:s:c:a:", opcionLarga, NULL)) != -1){
       switch(opcion){
           case 'N':
               //Si lee un -N, guarda ese valor en cantidadAves
               cantidadAves = optarg;
               // printf("cantidad aves: %s\n",cantidadAves);
               break;
           case 's':
           //Si lee un -s, guarda ese valor en w_s
               w_s = optarg;
               // printf("ws: %s\n",w_s);
               break;
           case 'c':
           //Si lee un -c, guarda ese valor en w_c
               w_c = optarg;
               // printf("wc: %s\n",w_c);
               break;
           case 'a':
           //Si lee un -a, guarda ese valor en w_a
               w_a = optarg;
               // printf("wa: %s\n",w_a);

           //estos casos son para manejo de errores! 
           case -1:
               break;
           case '?':
               break;
               exit(1);
           default:
               abort();
       }
   }

   //EN ESTA PARTE, COMO LAS VARIABLES SON  DE TIPO CONST CHAR*, HAY Q CONVERTIRLAS
    if(cantidadAves != NULL)
        numero_de_aves = atoi(cantidadAves);
    if(w_s != NULL)
        ws = atof(w_s);
    if(w_c != NULL)
        wc = atof(w_c);
    if(w_a != NULL)
        wa = atof(w_a);

	//srand (time(NULL));
	srand (time(NULL));

	// Limite inferior y superior para crear las aves horizontalmente respecto al centro
	// Ej: Si la ventana es de 640 x 640, el centro sera (320, 320),
	// el radio de creacion es 100,
	// entonces solo se podran crear pajaros en los rangos:
	// x: (220, 420)
	// y: (220, 420)
	int max_x = WIN_HEIGHT / 2 + RADIO_CREACION;
	int min_x = WIN_HEIGHT / 2 - RADIO_CREACION;
	
	int max_y = WIN_WIDTH / 2 + RADIO_CREACION;
	int min_y = WIN_WIDTH / 2 - RADIO_CREACION;
    
    for (int i = 0; i < numero_de_aves; ++i)
    {
    	int x = rand()%(max_x - min_x) + min_x;
    	int y = rand()%(max_y - min_y) + min_y;
        //x = 300;
        //y = 300;
    	//printf("%d %d\n", x, y); // Imprime las coordenadas obtenidas
		Aves.push_back(new Ave(x, y, i, &Aves, ws, wc, wa ));
		//Ave::Aves.push_back(new Ave(x, y, 0, i));
    }
    
    setup();

	//glutKeyboardFunc(handleKeypress); // Intento de manejar los eventos de presionar una tacla con la funcion handleKeypress

    glutMainLoop();
}